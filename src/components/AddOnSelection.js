import React        from 'react'
import PropTypes    from 'prop-types'
import RadioButton  from './shared/RadioButton'
import * as _       from 'lodash'

class AddOnSelection extends React.Component {
  render() {
    return(
      <div>
        {this.renderSelection()}
      </div>
    )
  }

  renderSelection() {
    return this.props.addOns.map((addOn, idx) => {
      const props = {
        name:       'selectedAddOn',
        value:      addOn.id,
        label:      addOn.value,
        isChecked:  addOn.id === this.props.selectedAddOn,
        onChange:   this.onChange.bind(this),
        disabled:   this.isDisabled(addOn.id)
      }
      return(
        <RadioButton key={`add-on-selection-${idx}`} {...props} />
      )
    })
  }

  onChange(e) {
    this.props.addonOnChange(e.target.value)
  }

  isDisabled(addOnId) {
    if (!this.props.selectedMenu) { return true }
    if (!this.props.inCompatibleAddOnsIds.length) { return false }
    return _.includes(this.props.inCompatibleAddOnsIds, parseInt(addOnId))
  }
}

AddOnSelection.propTypes = {
  addOns:                PropTypes.array.isRequired,
  selectedAddOn:         PropTypes.string,
  addonOnChange:         PropTypes.func.isRequired,
  inCompatibleAddOnsIds: PropTypes.array.isRequired,
  selectedMenu:          PropTypes.string
}

export default AddOnSelection