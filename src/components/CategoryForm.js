import React              from 'react'
import PropTypes          from 'prop-types'
import CategorySelection  from './CategorySelection'
import MenuSelection      from './MenuSelection'
import AddOnSelection     from './AddOnSelection'
import * as _             from 'lodash'

class CategoryForm extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      selectedCategory: null,
      selectedMenu: null,
      selectedAddOn: null
    }
  }

  render() {
    if (!this.props.selections.length) { return null }
    return(
      <div className="row">
        <div className="col-md-8 offset-md-4">
          <div className="mb-2">
            <h4>Meal Category</h4>
            {this.renderCategorySelections()}
          </div>
          <div className="mb-2">
            <h4>Meal Selection</h4>
            {this.renderMenuSelections()}
          </div>
          <div className="mb-2">
            <h4>Addon Selection</h4>
            {this.renderAddOns()}
          </div>
        </div>
      </div>
    )
  }

  renderCategorySelections() {
    const categories = this.props.selections[0] || []
    return(
      <CategorySelection 
        categories={categories}
        selectedCategory={this.state.selectedCategory}
        categoryOnChange={this.categoryOnChange.bind(this)} />
    )
  }

  renderMenuSelections() {
    const menus = this.props.selections[1] || []
    return(
      <MenuSelection 
        menus={menus}
        selectedMenu={this.state.selectedMenu}
        menuOnChange={this.menuOnChange.bind(this)}
        inCompatibleMenuIds={this.inCompatibleMenuIds()} />
    )
  }

  renderAddOns() {
    const addOns = this.props.selections[2] || []
    return(
      <AddOnSelection 
        addOns={addOns}
        selectedMenu={this.state.selectedMenu}
        selectedAddOn={this.state.selectedAddOn}
        addonOnChange={this.addonOnChange.bind(this)}
        inCompatibleAddOnsIds={this.inCompatibleAddOnsIds()} />
    )
  }

  categoryOnChange(value) {
    this.setState({ selectedCategory: value, selectedMenu: null, selectedAddOn: null })
  }

  menuOnChange(value) {
    this.setState({ selectedMenu: value })
  }

  addonOnChange(value) {
    this.setState({ selectedAddOn: value })
  }

  inCompatibleMenuIds() {
    if (!this.state.selectedCategory) { return [] }
    return this.props.categoryLookup[this.state.selectedCategory] || []
  }

  inCompatibleAddOnsIds() {
    if (!this.state.selectedMenu) { return [] }
    return this.props.categoryLookup[this.state.selectedMenu] || []
  }
}

CategoryForm.propTypes = {
  categoryLookup: PropTypes.object.isRequired,
  selections:     PropTypes.array.isRequired
}

export default CategoryForm