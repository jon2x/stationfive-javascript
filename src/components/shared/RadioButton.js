import React      from 'react'
import PropTypes  from 'prop-types'

class RadioButton extends React.Component {
  render() {
    return(
      <div className="form-check">
        <input 
          className="form-check-input" 
          type="radio" 
          name={this.props.name} 
          id={this.props.name} 
          value={this.props.value} 
          checked={this.props.isChecked} 
          onChange={this.props.onChange}
          disabled={this.props.disabled} />
        <label className="form-check-label">
          {this.props.label}
        </label>
      </div>
    )
  }
}

RadioButton.propTypes = {
  label:    PropTypes.string.isRequired,
  name:     PropTypes.string.isRequired,
  value:    PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  isChecked:PropTypes.bool
}

export default RadioButton