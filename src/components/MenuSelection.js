import React        from 'react'
import PropTypes    from 'prop-types'
import RadioButton  from './shared/RadioButton'
import * as _       from 'lodash'

class MenuSelection extends React.Component {
  render() {
    return(
      <div>
        {this.renderSelection()}
      </div>
    )
  }

  renderSelection() {
    return this.props.menus.map((menu, idx) => {
      const props = {
        name:       'selectedMenu',
        value:      menu.id,
        label:      menu.value,
        isChecked:  menu.id === this.props.selectedMenu,
        onChange:   this.onChange.bind(this),
        disabled:   this.isDisabled(menu.id)
      }
      return(
        <RadioButton key={`menu-selection-${idx}`} {...props} />
      )
    })
  }

  onChange(e) {
    this.props.menuOnChange(e.target.value)
  }

  isDisabled(menuId) {
    if (!this.props.inCompatibleMenuIds.length) { return true }
    return _.includes(this.props.inCompatibleMenuIds, parseInt(menuId))
  }
}

MenuSelection.propTypes = {
  menus:                PropTypes.array.isRequired,
  selectedMenu:         PropTypes.string,
  menuOnChange:         PropTypes.func.isRequired,
  inCompatibleMenuIds:  PropTypes.array.isRequired
}

export default MenuSelection