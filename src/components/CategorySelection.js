import React        from 'react'
import PropTypes    from 'prop-types'
import RadioButton  from './shared/RadioButton'

class CategorySelection extends React.Component {
  render() {
    return(
      <div>
        {this.renderSelection()}
      </div>
    )
  }

  renderSelection() {
    return this.props.categories.map((category, idx) => {
      const props = {
        name:       'selectedCategory',
        value:      category.id,
        label:      category.value,
        isChecked:  category.id === this.props.selectedCategory,
        onChange:   this.onChange.bind(this)
      }
      return(
        <RadioButton key={`category-selection-${idx}`} {...props} />
      )
    })
  }

  onChange(e) {
    this.props.categoryOnChange(e.target.value)
  }
}

CategorySelection.propTypes = {
  categories:       PropTypes.array.isRequired,
  selectedCategory: PropTypes.string,
  categoryOnChange: PropTypes.func.isRequired
}

export default CategorySelection