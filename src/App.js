import React, { Component } from 'react';
import CategoryForm         from './components/CategoryForm'
import * as fixtures        from './fixtures/mockup'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container">
        <CategoryForm selections={fixtures.selections} 
                      categoryLookup={fixtures.categoryLookup} />
      </div>
    );
  }
}

export default App;
